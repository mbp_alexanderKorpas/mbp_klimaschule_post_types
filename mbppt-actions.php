<?php
/* ===== Custom post type actions ====== */
function mbppt_action_post_type()
{

  $labels = array(
    'name' => __('actions', 'myblueplanet-post-types'),
    'singular_name' => __('action', 'myblueplanet-post-types'),
    'menu_name' => __('KSK actions', 'myblueplanet-post-types'),
    "all_items" => __('All actions', 'myblueplanet-post-types'),
    "add_new" => __('new action', 'myblueplanet-post-types'),
    "add_new_item" => __('add new action', 'myblueplanet-post-types'),
    "edit_item" => __('edit action', 'myblueplanet-post-types'),
    "new_item" => __('new action', 'myblueplanet-post-types'),
    "view_item" => __('view action', 'myblueplanet-post-types'),
    "search_items" => __('search', 'myblueplanet-post-types'),
  );

  $args = array(
    "labels" => $labels,
    'public' => true,
    'has_archive' => false,
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'thumbnail',
      'revisions',
      'templates'
    ),
    'menu_icon' => 'data:image/svg+xml;base64,' . base64_encode('<svg width="20" height="20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bolt" class="svg-inline--fa fa-bolt fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M296 160H180.6l42.6-129.8C227.2 15 215.7 0 200 0H56C44 0 33.8 8.9 32.2 20.8l-32 240C-1.7 275.2 9.5 288 24 288h118.7L96.6 482.5c-3.6 15.2 8 29.5 23.3 29.5 8.4 0 16.4-4.4 20.8-12l176-304c9.3-15.9-2.2-36-20.7-36z"></path></svg>'),
    "menu_position" => 6,
  );


  register_post_type('actions', $args);
}
add_action('init', 'mbppt_action_post_type');

/* create custom taxonomy SDGS */

function create_sdg_hierarchical_taxonomy()
{

  $labels = array(
    'name' => _x('SDGS', 'myblueplanet-post-types'),
    'singular_name' => _x('SDG', 'myblueplanet-post-types'),
    'search_items' =>  __('Search SDGS', 'myblueplanet-post-types'),
    'all_items' => __('All SDGS', 'myblueplanet-post-types'),
    'edit_item' => __('Edit SDG', 'myblueplanet-post-types'),
    'update_item' => __('Update SDG', 'myblueplanet-post-types'),
    'add_new_item' => __('Add New SDG', 'myblueplanet-post-types'),
    'new_item_name' => __('New SDG Name', 'myblueplanet-post-types'),
    'menu_name' => __('SDGS', 'myblueplanet-post-types')
  );

  register_taxonomy('sdgs', array('actions'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true
  ));
}
add_action('init', 'create_sdg_hierarchical_taxonomy', 0);

/* create custom taxonomy KSK topics */

function create_ksk_topics_taxonomy()
{

  $labels = array(
    'name' => _x('KSK Topics', 'myblueplanet-post-types'),
    'singular_name' => _x('KSK Topic', 'myblueplanet-post-types'),
    'search_items' =>  __('KSK Topics', 'myblueplanet-post-types'),
    'all_items' => __('All KSK Topics', 'myblueplanet-post-types'),
    'edit_item' => __('Edit Topic', 'myblueplanet-post-types'),
    'update_item' => __('Update Topic', 'myblueplanet-post-types'),
    'add_new_item' => __('Add Topic', 'myblueplanet-post-types'),
    'new_item_name' => __('New Topic Name', 'myblueplanet-post-types'),
    'menu_name' => __('Topics', 'myblueplanet-post-types')
  );

  register_taxonomy('ksk_topics', array('actions'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true
  ));
}
add_action('init', 'create_ksk_Topics_taxonomy', 0);

/* ===== use custom flatsome template file inluded in this plugin for actions ====== */
add_filter('single_template', 'mbppt_actions_template');

function mbppt_actions_template($single)
{

  global $post;

  // Checks for single template by post type
  if ($post->post_type == 'actions') {
    return WP_PLUGIN_DIR . '/myblueplanet-post-types/mbppt-action-template.php';
  }

  return $single;
}

/* ===== Actions output function for use in loops ===== */
function mbppt_ksk_output()
{ ?>
  <a class="portfolio-link" href="<?php the_permalink(); ?>">
    <div class="mbppt-thumbnail" style="background-image: url('<?php the_post_thumbnail_url("archive_thumbnail") ?>')">
      <?php echo mbppt_action_pledge_svg() ?>
    </div>
    <h3><?php the_title() ?></h3>
  </a>
<?php }

/* ===== Create ksk archive loop shortcode ====== */
function mbppt_ksk_archive()
{
  ob_start();

  $query_recipes = new WP_Query(
    array(
      'post_type' => 'actions',
      'posts_per_page' => -1,
      'order' => 'asc'
    )
  );
  ?>
  <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="mbppt-ajax-filter">
    <?php
    if ($terms = get_terms(array('taxonomy' => 'sdgs', 'orderby' => 'slug'))) :
      echo '<div class="mbppt-stg-filter">';
      foreach ($terms as $term) :
        echo '<label class="mbppt-check-container checkbox"><input type="checkbox" name="categoryfilter[]" value="' . $term->term_id . '"><span class="mbppt-checkmark ' . $term->slug . '"></span></label>'; // ID of the category as the value of an option
      endforeach;
      echo '</div>';
    endif;

    ?>
    <div class="mbppt-action-ajax-filter">
      <?php
      if ($terms = get_terms(array('taxonomy' => 'ksk_topics', 'orderby' => 'slug'))) :
        echo '<div class="mbppt-topic-filter">';
        foreach ($terms as $term) :
          echo '<label class="mbppt-check-container tag checkbox"><input type="checkbox" name="topicfilter[]" value="' . $term->term_id . '"><span class="mbppt-tagmark ' . $term->slug . '">' . $term->name . '</span></label>'; // ID of the category as the value of an option
        endforeach;
        echo '</div>';
      endif;
      ?>
      <div class="btn_wrapper"><button id="mbppt_btn" class="button mbppt-button primary is-outline"><?php _e('Apply filter', 'myblueplanet-post-types') ?></button></div>
      <input type="hidden" name="action" value="action_filter">
    </div>
  </form>
  <div class="mbppt-archive" id="response">
    <div class="mbppt-hidden" id="mbppt-loader-wrapper"></div>
    <?php while ($query_recipes->have_posts()) : $query_recipes->the_post();

      echo mbppt_ksk_output();

    endwhile;
    wp_reset_query(); ?>
  </div>
  <?php
  // inject js function into footer area (function defined in mbp_klimaschule_post_types.php)
  add_action('wp_footer', 'mbppt_js');

  return ob_get_clean();
}
add_shortcode('mbppt_ksk_archive', 'mbppt_ksk_archive');

// Prossess ajax request
add_action('wp_ajax_action_filter', 'mbppt_actions_filter_function'); // wp_ajax_{ACTION HERE} 
add_action('wp_ajax_nopriv_action_filter', 'mbppt_actions_filter_function');

function mbppt_actions_filter_function()
{
  // for taxonomies / categories
  $args = array(
    'post_type' => 'actions',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'order' => 'asc',
    'tax_query' => array(
      'relation' => 'AND',
    )
  );
  // if category filled, use category in query
  if (isset($_POST['categoryfilter']))
    $args['tax_query'][] = array(
      array(
        'taxonomy' => 'sdgs',
        'field' => 'id',
        'terms' => $_POST['categoryfilter'],
        'operator' => 'IN'
      ),
    );
  // if topic filled, use topic in query
  if (isset($_POST['topicfilter']))
    $args['tax_query'][] = array(
      array(
        'taxonomy' => 'ksk_topics',
        'field' => 'id',
        'terms' => $_POST['topicfilter'],
        'operator' => 'IN'
      ),
    );
  // if keyword filled, use keyword in query
  /*
  if (isset($_POST['keyword']))
    $args['s'] = $_POST['keyword'];
    */

  $query = new WP_Query($args);

  ?>
  <div class="mbppt-hidden" id="mbppt-loader-wrapper"></div>
  <?php
  if ($query->have_posts()) :
    while ($query->have_posts()) : $query->the_post();

      echo mbppt_ksk_output();

    endwhile;
    wp_reset_postdata();
  else :
      echo '<h3 class="noPostsFoundh3">'._e('No posts found', 'myblueplanet-post-types').'</h3>';
  endif;

  die();
}
/* ===== Create ksk rand loop ====== */
function mbppt_ksk_rand()
{
  $exclude_ids = array(get_the_ID());
  $terms = get_the_terms(get_the_ID(), "ksk_topics");
  if ($terms) {
    foreach ($terms as $t) {
      $term_array[] = $t->term_id;
    }
  }

  $query_actions = new WP_Query(
    array(
      'post_type' => 'actions',
      'orderby' => 'rand',
      'posts_per_page' => 3,
      'post__not_in' => $exclude_ids,
      'tax_query' => array(
        array(
          'taxonomy' => 'ksk_topics',
          'field' => 'id',
          'terms' => $term_array,
          'operator' => 'in'
        ),
      )
    )
  );
  ?>
  <h2 style="text-align: left;"><?php _e('More actions like this', 'myblueplanet-post-types'); ?></h2>
  <div class="mbppt-archive" id="response">
    <?php while ($query_actions->have_posts()) : $query_actions->the_post();
      echo mbppt_ksk_output();

    endwhile;
    wp_reset_query(); ?>
  </div>
  <?php
  // inject js function into footer area (function defined in mbp_klimaschule_post_types.php)
  add_action('wp_footer', 'mbppt_js');

  // return ob_get_clean();
}
add_shortcode('mbppt_ksk_rand', 'mbppt_ksk_rand');


// Email Pledges
global $mbppt_db_version;
$mbppt_db_version = '1.0';

function mbppt_activate()
{

  global $wpdb;
  global $mbppt_db_version;


  $table_name = $wpdb->prefix . 'mbppt_action_pledge';

  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE $table_name (
      `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `post_id` int NOT NULL,
	    `email` VARCHAR(50) not null
    ) $charset_collate;";

  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  dbDelta($sql);

  add_option('mbppt_db_version', $mbppt_db_version);
}
register_activation_hook(__FILE__, 'mbppt_activate');

function mbppt_action_pledge_svg()
{
  global $wpdb;
  $tablename = $wpdb->prefix . "mbppt_action_pledge";
  $the_post_id = get_the_id();
  $post_ids = $wpdb->get_col(
    $wpdb->prepare(
      "
      SELECT id
      FROM $tablename
      WHERE post_id = %s
      ",
      $the_post_id
    )
  );
  ?>
  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 527 527" style="enable-background:new 0 0 527 527;" xml:space="preserve">
    <g>
      <path class="st0" d="M263.5,519.5c-68.4,0-132.7-26.6-181-75c-48.4-48.4-75-112.6-75-181s26.6-132.7,75-181s112.6-75,181-75
		                      s132.7,26.6,181,75c48.4,48.4,75,112.6,75,181s-26.6,132.7-75,181S331.9,519.5,263.5,519.5z" />
      <path class="st1" d="M263.5,15c33.6,0,66.1,6.6,96.7,19.5c29.6,12.5,56.2,30.4,79,53.3c22.8,22.8,40.7,49.4,53.3,79
		                      c13,30.6,19.5,63.2,19.5,96.7s-6.6,66.1-19.5,96.7c-12.5,29.6-30.4,56.2-53.3,79c-22.8,22.8-49.4,40.7-79,53.3
		                      c-30.6,13-63.2,19.5-96.7,19.5s-66.1-6.6-96.7-19.5c-29.6-12.5-56.2-30.4-79-53.3c-22.8-22.8-40.7-49.4-53.3-79
		                      c-13-30.6-19.5-63.2-19.5-96.7s6.6-66.1,19.5-96.7c12.5-29.6,30.4-56.2,53.3-79c22.8-22.8,49.4-40.7,79-53.3
		                      C197.4,21.6,229.9,15,263.5,15 M263.5,0C118,0,0,118,0,263.5S118,527,263.5,527S527,409,527,263.5S409,0,263.5,0L263.5,0z" />
    </g>
    <path id="SVGID_x5F_1_x5F_" class="st2" d="M263.5,470C149.5,470,57,377.5,57,263.5S149.5,57,263.5,57S470,149.5,470,263.5
	                      S377.5,470,263.5,470z" />
    <text>
      <textPath text-anchor="middle" xlink:href="#SVGID_x5F_1_x5F_" startOffset="50%">
        <tspan class="st1 st3" style="font-size:40px;"><?php the_title() ?></tspan>
      </textPath>
    </text>
    <g>
      <circle class="st1" cx="263.5" cy="263.5" r="183" />
      <path class="st1" d="M263.5,88c46.9,0,90.9,18.3,124.1,51.4S439,216.6,439,263.5s-18.3,90.9-51.4,124.1S310.4,439,263.5,439
		                      s-90.9-18.3-124.1-51.4S88,310.4,88,263.5s18.3-90.9,51.4-124.1S216.6,88,263.5,88 M263.5,73C158.3,73,73,158.3,73,263.5
		                      S158.3,454,263.5,454S454,368.7,454,263.5S368.7,73,263.5,73L263.5,73z" />
    </g>
    <text text-anchor="middle" transform="matrix(1 0 0 1 263.5 310)" class="st0 st4"><?php echo rtrim(count($post_ids)); ?></text>
    <text text-anchor="middle" transform="matrix(1 0 0 1 263.5 360)" class="st0 st5"><?php if (count($post_ids) == 1) {
                                                                                        _e("Supporter", "myblueplanet-post-types");
                                                                                      } else {
                                                                                        _e("Supporters", "myblueplanet-post-types");
                                                                                      } ?></text>
    <path id="SVGID_x5F_2_x5F_" class="st2" d="M263.5,29C134,29,29,134,29,263.5S134,498,263.5,498S498,393,498,263.5S393,29,263.5,29z
	                      " />
    <text>
      <textPath text-anchor="middle" xlink:href="#SVGID_x5F_2_x5F_" startOffset="50%">
        <tspan class="st1" style="font-size:40px;"><?php echo _e('Support this action now!', 'myblueplanet-post-types'); ?></tspan>
      </textPath>
    </text>
  </svg>
<?php
}

function mbppt_action_pledge_form()
{
  ?>
  <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" id="mbppt-action-pledge-form" method="POST">
    <input placeholder="<?php _e('E-mail adress', 'myblueplanet-post-types') ?>" type="text" name="email" id="">
    <input type="hidden" name="post_id" value="<?php the_id(); ?>" id="">
    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
    <input type="hidden" name="action" value="action_pledge">
    <button class="button mbppt-button primary is-outline"><?php _e('I support that!', 'myblueplanet-post-types') ?></button>
  </form>
<?php
}
add_action('wp_ajax_action_pledge', 'mbppt_action_pledge_function'); // wp_ajax_{ACTION HERE} 
add_action('wp_ajax_nopriv_action_pledge', 'mbppt_action_pledge_function');

function mbppt_action_pledge_function()
{

  global $wpdb;
  $form_email = $_POST['email'];
  $form_post_id = $_POST['post_id'];
  $tablename = $wpdb->prefix . "mbppt_action_pledge";
  $emails = $wpdb->get_col(
    $wpdb->prepare(
      "
      SELECT id
      FROM $tablename
      WHERE email = %s
      ",
      $form_email
    )

  );
  $post_ids = $wpdb->get_col(
    $wpdb->prepare(
      "
      SELECT id
      FROM $tablename
      WHERE post_id = %s
      ",
      $form_post_id
    )
  );
  $result = array_intersect($post_ids, $emails);

  if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response'])) {

    // Build POST request:
    $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
    $recaptcha_secret = '6LfxVqoUAAAAAL-By3GAMuJ8HtwL4tDkz9Sp7NeR';
    $recaptcha_response = $_POST['recaptcha_response'];

    // Make and decode POST request:
    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
    $recaptcha = json_decode($recaptcha);
    if ($recaptcha->score >= 0.5) {

      if (isset($form_email) && count($result) == 0 && filter_var($form_email, FILTER_VALIDATE_EMAIL)) {
        $wpdb->query(
          $wpdb->prepare(
            "
            INSERT IGNORE INTO $tablename
            ( post_id, email )
            VALUES ( %d, %s )
            ",
            array($form_post_id, $form_email)
          )
        );
      } else {
        header("HTTP/1.0 400 Bad Request");
        exit();
      }
    } else {
      header("HTTP/1.0 400 Recaptcha");
      exit();
    }
  } else {
    header("HTTP/1.0 400 Bad Request");
    exit();
  }
};

function mbppt_pledge_total()
{
  ob_start();
  global $wpdb;
  $post_id = get_the_ID();
  $pledge_goal = get_post_meta($post_id, 'pledge_goal', true);
  $tablename = $wpdb->prefix . "mbppt_action_pledge";
  $post_ids = $wpdb->get_col(
    "
      SELECT id
      FROM $tablename
      "
  );
  ?>
  <a href="#mbppt-ajax-filter" class="mbppt-pledge-bar" >
    <div class="mbppt-pledge-bar-fill" style="width:<?php echo (100 / $pledge_goal) * count($post_ids) . '%'; ?>">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 527 659.3" style="enable-background:new 0 0 527 527;" xml:space="preserve">
        <g>
          <path class="st0" d="M263.5,650.2c-15.5-11-71.3-52.3-126.4-112.4c-37.8-41.1-67.8-82.9-89.3-124.2C21,362.4,7.5,311.9,7.5,263.5
			  c0-68.4,26.6-132.7,75-181c48.4-48.4,112.6-75,181-75c68.4,0,132.7,26.6,181,75c48.4,48.4,75,112.6,75,181
			  c0,48.4-13.5,98.9-40.2,150.1c-21.5,41.3-51.6,83-89.3,124.2C334.8,597.9,279,639.1,263.5,650.2z" />
          <path class="st1" d="M263.5,15c33.6,0,66.1,6.6,96.7,19.5c29.6,12.5,56.2,30.4,79,53.3c22.8,22.8,40.7,49.4,53.3,79
			  c13,30.6,19.5,63.2,19.5,96.7c0,47.1-13.2,96.4-39.3,146.5c-21.2,40.6-50.8,81.8-88.1,122.5c-50.4,55-101.5,94.2-121.1,108.5
			  c-19.6-14.3-70.7-53.4-121.1-108.5c-37.3-40.6-66.9-81.8-88.1-122.5C28.2,359.9,15,310.6,15,263.5c0-33.6,6.6-66.1,19.5-96.7
			  c12.5-29.6,30.4-56.2,53.3-79c22.8-22.8,49.4-40.7,79-53.3C197.4,21.6,229.9,15,263.5,15 M263.5,0C118,0,0,118,0,263.5
			  c0,217.8,263.5,395.8,263.5,395.8S527,481.3,527,263.5C527,118,409,0,263.5,0L263.5,0z" />
        </g>

        <path id="SVGID_x5F_1_x5F_" class="st2" d="M263.5,470C149.5,470,57,377.5,57,263.5S149.5,57,263.5,57S470,149.5,470,263.5
	                      S377.5,470,263.5,470z" />
        <text>
          <textPath text-anchor="middle" xlink:href="#SVGID_x5F_1_x5F_" startOffset="50%">
            <tspan class="st1 st3" style="font-size:40px;"><?php the_title() ?></tspan>
          </textPath>
        </text>
        <g>
          <circle class="st1" cx="263.5" cy="263.5" r="183" />
          <path class="st1" d="M263.5,88c46.9,0,90.9,18.3,124.1,51.4S439,216.6,439,263.5s-18.3,90.9-51.4,124.1S310.4,439,263.5,439
		                      s-90.9-18.3-124.1-51.4S88,310.4,88,263.5s18.3-90.9,51.4-124.1S216.6,88,263.5,88 M263.5,73C158.3,73,73,158.3,73,263.5
		                      S158.3,454,263.5,454S454,368.7,454,263.5S368.7,73,263.5,73L263.5,73z" />
        </g>
        <text text-anchor="middle" transform="matrix(1 0 0 1 263.5 310)" class="st0 st4"><?php echo rtrim(count($post_ids)); ?></text>
        <text text-anchor="middle" transform="matrix(1 0 0 1 263.5 360)" class="st0 st5"><?php if (count($post_ids) == 1) {
                                                                                            _e("Supporter", "myblueplanet-post-types");
                                                                                          } else {
                                                                                            _e("Supporters", "myblueplanet-post-types");
                                                                                          } ?></text>
        <path id="SVGID_x5F_2_x5F_" class="st2" d="M263.5,29C134,29,29,134,29,263.5S134,498,263.5,498S498,393,498,263.5S393,29,263.5,29z
	                      " />
        <text>
          <textPath text-anchor="middle" xlink:href="#SVGID_x5F_2_x5F_" startOffset="50%">
            <tspan class="st1" style="font-size:40px;"><?php echo _e('Support our actions now!', 'myblueplanet-post-types'); ?></tspan>
          </textPath>
        </text>
      </svg>
    </div>
  </a>
  <?php return ob_get_clean();
}
add_shortcode('mbppt_pledge_total', 'mbppt_pledge_total');
