<?php
/*
Template name: Page - Full Width
*/
get_header(); ?>

<?php do_action('flatsome_before_page');
?>

<div id="content" role="main" class="content-area">
  <?php while (have_posts()) : the_post(); ?>
    <!-- Header with featured image -->
    <div class="mbppt-header action" style="background-image: url('<?php the_post_thumbnail_url("post_template"); ?>')"></div> <!--  -->
    <div class="mbppt-pledge-wrapper">
      <div class="container mbppt-grid-pledge">
        <?php echo mbppt_action_pledge_svg() ?>
        <?php echo mbppt_action_pledge_form(); ?>
      </div>
      
    </div>
    <div class="mbppt-back-btn-wrapper"><a class="button primary is-outline" href="https://www.myblueplanet.ch/klimaschutzaktionen/"><?php _e('Back to the Overview', 'myblueplanet-post-types') ?></a></div>
    <h1 class="container"><?php the_title() ?></h1>
    <?php
    if ($terms = get_the_terms(get_the_ID(), "ksk_topics")) :
      echo '<div class="mbppt-topic-filter mbppt-center">';
      foreach ($terms as $term) :
        echo '<label class="mbppt-check-container no-interaction tag checkbox"><span class="mbppt-tagmark ' . $term->slug . '">' . $term->name . '</span></label>'; // ID of the category as the value of an option
      endforeach;
      echo '</div>';
    endif;
    ?>
    <div class="mbppt-grid-action container">
      <section class="mbppt-action-content">
        <?php the_content(); ?>
      </section>
      <section class="mbppt-sidebar">
        <?php
        echo mbppt_ksk_rand();
        ?>
      </section>
    </div>
  <?php endwhile; ?>
</div>


<?php do_action('flatsome_after_page');

// inject js function into footer area (function defined in mbp_klimaschule_post_types.php)
add_action('wp_footer', 'mbppt_js');


get_footer(); ?>