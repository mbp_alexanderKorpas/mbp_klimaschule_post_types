<?php

/* ===== use custom flatsome template file inluded in this plugin for actions ====== */
add_filter('single_template', 'mbppt_actions_template');

function mbppt_actions_template($single)
{

    global $post;

    // Checks for single template by post type
    if ($post->post_type == 'bildungsinputs') {
        return WP_PLUGIN_DIR . '/mbp_klimaschule_post_types/mbppt-educational-inputs-template.php';
    }

    return $single;
}

/* ===== eduInput output function for use in loops ===== */
function mbppt_eduInput_output()
{ ?>
    <a class="input-link" href="<?php the_permalink(); ?>">
        <div class="mbppt-thumbnail" style="background-image: url('<?php the_post_thumbnail_url("archive_thumbnail") ?>')">
        </div>
        <h3><?php the_title() ?></h3>
    </a>
<?php }


/* ===== Create recipes archive loop shortcode ====== */
function mbppt_educational_inputs_archive()
{
    ob_start();

    $query_eduInput = new WP_Query(
        array(
            'post_type' => 'bildungsinputs',
            'posts_per_page' => -1
        )
    );
    ?>
    <div id="main-eduInput-content">
        <form autocomplete="off" action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" class="mbppt-eduInput-ajax-filter" id="mbppt-ajax-filter">
            <?php
            if ($terms = get_terms(array('taxonomy' => 'unterrichtsformen'))) :
                echo '<div class="check-item-heading"><p>Unterrichtsformen</p></div>';
                foreach ($terms as $term) :
                    echo '<div class="check-item"><input class="checkbox-input" id="'.$term->term_id.'" type="checkbox" name="unterrichtsformenFilter[]" value="' . $term->term_id . '">
                            <label for="'.$term->term_id.'">'.$term->name.'</label>
                          </div>'; // ID of the category as the value of an option
                endforeach;
                echo '<div class="separator"></div>';
            endif;
            if ($terms = get_terms(array('taxonomy' => 'schulstufen'))) :
                echo '<div class="check-item-heading"><p>Schulstufen</p></div>';
                foreach ($terms as $term) :
                    echo '<div class="check-item"><input class="checkbox-input" id="'.$term->term_id.'" type="checkbox" name="schulstufenFilter[]" value="' . $term->term_id . '"><label for="'.$term->term_id.'">'.$term->name.'</label></div>'; // ID of the category as the value of an option
                endforeach;
                echo '<div class="separator"></div>';
            endif;
            if ($terms = get_terms(array('taxonomy' => 'themen'))) :
                echo '<div class="check-item-heading"><p>Themen</p></div>';
                foreach ($terms as $term) :
                    echo '<div class="check-item"><input class="checkbox-input" id="'.$term->term_id.'" type="checkbox" name="themenFilter[]" value="' . $term->term_id . '"><label for="'.$term->term_id.'">'.$term->name.'</label></div>'; // ID of the category as the value of an option
                endforeach;
                echo '<div class="separator"></div>';
            endif;
            if ($terms = get_terms(array('taxonomy' => 'faecher'))) :
                echo '<div class="check-item-heading"><p>Fächer</p></div>';
                foreach ($terms as $term) :
                    echo '<div class="check-item"><input class="checkbox-input" id="'.$term->term_id.'" type="checkbox" name="faecherFilter[]" value="' . $term->term_id . '"><label for="'.$term->term_id.'">'.$term->name.'</label></div>'; // ID of the category as the value of an option
                endforeach;
                echo '<div class="separator"></div>';
            endif;
            if ($terms = get_terms(array('taxonomy' => 'regions'))) :
                echo '<div class="check-item-heading"><p>Regionen</p></div>';
                foreach ($terms as $term) :
                    echo '<div class="check-item"><input class="checkbox-input" id="'.$term->term_id.'" type="checkbox" name="regionFilter[]" value="' . $term->term_id . '">
                            <label for="'.$term->term_id.'">'.$term->name.'</label>
                          </div>'; // ID of the category as the value of an option
                endforeach;
            endif;
            ?>
            <!--Button not needed - form auto triggers on click. <button class="mbppt-search"><i class="icon-search"></i></button>-->
            <input type="hidden" name="action" value="eduInput_filter">
        </form>
        <div class="mbppt-archive" id="response">
            <div class="mbppt-hidden" id="mbppt-loader-wrapper"></div>
            <?php while ($query_eduInput->have_posts()) : $query_eduInput->the_post();
                echo mbppt_eduInput_output();

            endwhile;
            wp_reset_query(); ?>
        </div>
    </div>
    <?php
    // inject js function into footer area (function defined in mbp_klimaschule_post_types.php)
    add_action('wp_footer', 'mbppt_js');

    return ob_get_clean();
}

add_shortcode('mbppt_educational_inputs_archive', 'mbppt_educational_inputs_archive');
// Prossess ajax request
add_action('wp_ajax_eduInput_filter', 'mbppt_eduInput_filter_function'); // wp_ajax_{ACTION HERE}
add_action('wp_ajax_nopriv_eduInput_filter', 'mbppt_eduInput_filter_function');

function mbppt_eduInput_filter_function()
{
    // for taxonomies / categories
    $args = array(
        'post_type' => 'bildungsinputs',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'order' => 'desc',
        'tax_query' => array(
            'relation' => 'AND',
        )
    );
    // if category filled, use category in query
    if (isset($_POST['regionFilter']))
        $args['tax_query'][] = array(
            array(
                'taxonomy' => 'regions',
                'field' => 'id',
                'terms' => $_POST['regionFilter'],
                'operator' => 'IN'
            ),
        );
    // if topic filled, use topic in query
    if (isset($_POST['unterrichtsformenFilter']))
        $args['tax_query'][] = array(
            array(
                'taxonomy' => 'unterrichtsformen',
                'field' => 'id',
                'terms' => $_POST['unterrichtsformenFilter'],
                'operator' => 'IN'
            ),
        );
    if (isset($_POST['themenFilter']))
        $args['tax_query'][] = array(
            array(
                'taxonomy' => 'themen',
                'field' => 'id',
                'terms' => $_POST['themenFilter'],
                'operator' => 'IN'
            ),
        );
    if (isset($_POST['schulstufenFilter']))
        $args['tax_query'][] = array(
            array(
                'taxonomy' => 'schulstufen',
                'field' => 'id',
                'terms' => $_POST['schulstufenFilter'],
                'operator' => 'IN'
            ),
        );

    if (isset($_POST['faecherFilter']))
        $args['tax_query'][] = array(
            array(
                'taxonomy' => 'faecher',
                'field' => 'id',
                'terms' => $_POST['faecherFilter'],
                'operator' => 'IN'
            ),
        );

    // if keyword filled, use keyword in query
    /*
    if (isset($_POST['keyword']))
      $args['s'] = $_POST['keyword'];
      */

    $query = new WP_Query($args);

    ?>
    <div class="mbppt-hidden" id="mbppt-loader-wrapper"></div>
    <?php
    if ($query->have_posts()) :
        while ($query->have_posts()) : $query->the_post();

            echo mbppt_eduInput_output();

        endwhile;
        wp_reset_postdata();
    else :
        echo '<h3 class="noPostsFoundh3">'._e('No posts found', 'myblueplanet-post-types').'</h3>';
    endif;

    die();
}


