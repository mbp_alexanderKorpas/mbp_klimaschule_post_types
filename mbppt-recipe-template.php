<?php
/*
Template name: Page - Full Width
*/
get_header(); ?>

<?php do_action('flatsome_before_page'); ?>

<div id="content" role="main" class="content-area">
  <?php while (have_posts()) : the_post(); ?>
    <!-- Header with featured image -->
    <div class="mbppt-header recipe" style="background-image: url('<?php the_post_thumbnail_url("post_template")?>')"></div>
    <div class="mbppt-back-btn-wrapper"><a class="button primary is-outline" href="https://www.myblueplanet.ch/klimamenue/"><?php _e('Back to the Overview', 'myblueplanet-post-types') ?></a></div>
    <h1><?php the_title() ?></h1>
    <!-- Season selection form for CO2 calculation -->
    <form autocomplete="off" id="mbppt_form" name="mbppt_form" class="mbppt-container">
      <label class="mbppt-ckeck-container"><?php _e('Summer', 'myblueplanet-post-types'); ?>
        <input type="radio" checked="checked" name="radio" value="1">
        <span class="mbppt-radiomark"></span>
      </label>

      <label class="mbppt-ckeck-container"><?php _e('Autum', 'myblueplanet-post-types'); ?>
        <input type="radio" name="radio" value="2">
        <span class="mbppt-radiomark"></span>
      </label>

      <label class="mbppt-ckeck-container"><?php _e('Winter', 'myblueplanet-post-types'); ?>
        <input type="radio" name="radio" value="3">
        <span class="mbppt-radiomark"></span>
      </label>

      <label class="mbppt-ckeck-container"><?php _e('Spring', 'myblueplanet-post-types'); ?>
        <input type="radio" name="radio" value="4">
        <span class="mbppt-radiomark"></span>
      </label>
    </form>
    <!-- Recipe stats -->
    <div class="mbppt-container mbppt-grid-3 mbppt-stats">
      <!-- CO2 stats will be hidden/ displayed according to the selection in the form using js -->
      <p id="co2">
        <img class="mbppt-icon" src="<?php echo get_site_url() ?>\wp-content\plugins\myblueplanet-post-types\assets\icons\outline-cloud-24px.svg" alt="CO2">
        <span class="co2-text-1">
          <?php
          echo get_post_meta($post->ID, 'co2_summer', true) . 'g CO<sub>2</sub> ';
          echo '(' . (-1* (100 - round( 100 / 1600 * get_post_meta($post->ID, 'co2_summer', true))) . '%' . '<sup>1</sup>)');
          ?>
        </span>
        <span class="co2-text-2 hidden"> 
          <?php
          echo get_post_meta($post->ID, 'co2_autum', true) . 'g CO<sub>2</sub> ';
          echo '(' . (-1* (100 - round( 100 / 1600 * get_post_meta($post->ID, 'co2_autum', true))) . '%' . '<sup>1</sup>)');  
          ?>
        </span>
        <span class="co2-text-3 hidden">
          <?php
          echo get_post_meta($post->ID, 'co2_winter', true) . 'g CO<sub>2</sub> ';
          echo '(' . (-1* (100 - round( 100 / 1600 * get_post_meta($post->ID, 'co2_winter', true))) . '%' . '<sup>1</sup>)');
          ?>
        </span>
        <span class="co2-text-4 hidden">
          <?php
          echo get_post_meta($post->ID, 'co2_spring', true) . 'g CO<sub>2</sub> ';
          echo '(' . (-1* (100 - round( 100 / 1600 * get_post_meta($post->ID, 'co2_spring', true))) . '%' . '<sup>1</sup>)');
          ?>
        </span>
      </p>
      <!-- time stats -->
      <p id="time">
        <img class="mbppt-icon" src="<?php echo get_site_url() ?>\wp-content\plugins\myblueplanet-post-types\assets\icons\outline-av_timer-24px.svg" alt="Time">
        <span>
          <?php
          echo get_post_meta($post->ID, 'time', true);
          ?>
        </span>
      </p>
      <!-- cost stats -->
      <p id="cost">
        <img class="mbppt-icon" src="<?php echo get_site_url() ?>\wp-content\plugins\myblueplanet-post-types\assets\icons\outline-attach_money-24px.svg" alt="Cost">
        <span>
          <?php
          echo get_post_meta($post->ID, 'cost', true);
          ?>
        </span>
      </p>
    </div>
    <?php the_content(); ?>
    <ol class="container">
  <li><?php _e('in comparison to the average conventional meal','myblueplanet-post-types'); echo ': '; ?><a target="_blank" href="https://eaternity.org/meals/">https://eaternity.org/meals/</a></li>
</ol>
</div>
  <?php endwhile;
?>

<?php do_action('flatsome_after_page');

// inject js function into footer area (function defined in mbp_klimaschule_post_types.php)
add_action('wp_footer', 'mbppt_js');


get_footer(); ?>