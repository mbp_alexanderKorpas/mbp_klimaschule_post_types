<?php

/**
Plugin Name: myblueplanet post types
Description: Custom post types and loops for Klimanenu
Domain Path: /languages
text domain: myblueplanet-post-types
 */

/*include WP_PLUGIN_DIR . '/mbp_klimaschule_post_types/mbppt-recipes.php';
include WP_PLUGIN_DIR . '/mbp_klimaschule_post_types/mbppt-actions.php';*/
include WP_PLUGIN_DIR . '/mbp_klimaschule_post_types/mbppt-educational-inputs.php';

/* ===== activate Flatsome UX Builder support for post types ====== */
add_action('init', function () {
  add_ux_builder_post_type('bildungsinputs');
});

/* =====  include custom styles ====== */
function mbppt_scripts()
{
  wp_register_style('mbppt-styles',  plugin_dir_url(__FILE__) . 'assets/scss/mbppt-style.css', array(), '5.3.0');
  wp_enqueue_style('mbppt-styles');
}
add_action('wp_enqueue_scripts', 'mbppt_scripts');

/*===== Add image sizes =====*/
add_image_size('archive_thumbnail', 9999, 400);
add_image_size('post_thumbnail', 1080);

add_filter('image_size_names_choose', 'wpshout_custom_sizes');
function wpshout_custom_sizes($sizes)
{
  return array_merge($sizes, array(
    'archive_thumbnail' => __('Archive Thumbnail'),
    'post_thumbnail' => __('Post Thumbnail'),
  ));
}

/* ===== JS ====== */
function mbppt_js()
{
  ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <!-- js and ajax for both recipes and actions -->
  <script type="text/javascript">
      $(function(){
          $('.checkbox-input').on('change',function(e){
              e.preventDefault();
              $('#mbppt-ajax-filter').submit();
          });
      });

    // Portfolio Filter AJAX
    $('#mbppt-ajax-filter').submit(function() {
      var filter = $('#mbppt-ajax-filter');
      $.ajax({
        url: filter.attr('action'),
        data: filter.serialize(), // form data
        type: filter.attr('method'), // POST
        beforeSend: function(xhr) {
          $('#mbppt-loader-wrapper').removeClass('mbppt-hidden'); // showing a white overlay
        },
        success: function(data) {
          $('#mbppt-loader-wrapper').addClass('mbppt-hidden'); // hidding the white overlay again
          $('#response').html(data); // insert data
        }
      });
      return false;
    });
  </script>
<?php
};