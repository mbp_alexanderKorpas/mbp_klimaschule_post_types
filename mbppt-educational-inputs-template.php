<?php
/*
Template name: Page - Full Width
*/
get_header(); ?>

<?php do_action('flatsome_before_page'); ?>
    <!-- Header with featured image -->
    <div class="mbppt-header recipe" style="background-image: url('<?php the_post_thumbnail_url("post_template")?>')"></div>
<div id="content" class="row" role="main" class="content-area">
<?php while (have_posts()) : the_post(); ?>
    <div class="mbppt-back-btn-wrapper"><a class="button primary is-outline" href="https://www.klimaschule.ch/bildungskatalog/"><?php _e('Back to the Overview', 'myblueplanet-post-types') ?></a></div>
    <h1><?php the_title() ?></h1>
    <?php the_content(); ?>
</div>
<?php endwhile;
?>
<script>
    $('.check-item label').click(function(){
        $('#mbppt-ajax-filter').submit();
    })
</script>

<?php do_action('flatsome_after_page');

// inject js function into footer area (function defined in mbp_klimaschule_post_types.php)
add_action('wp_footer', 'mbppt_js');


get_footer(); ?>