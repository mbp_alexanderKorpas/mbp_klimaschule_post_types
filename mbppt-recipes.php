<?php
/* ===== Custom post type recepies ====== */
function mbppt_recipe_post_type()
{
  $labels = array(
    'name' => __('recipes', 'myblueplanet-post-types'),
    'singular_name' => __('recipe', 'myblueplanet-post-types'),
    'menu_name' => __('climate menu', 'myblueplanet-post-types'),
    "all_items" => __('All recipies', 'myblueplanet-post-types'),
    "add_new" => __('New recipe', 'myblueplanet-post-types'),
    "add_new_item" => __('Add new recipe', 'myblueplanet-post-types'),
    "edit_item" => __('edit recipe', 'myblueplanet-post-types'),
    "new_item" => __('new recipe', 'myblueplanet-post-types'),
    "view_item" => __('view recipe', 'myblueplanet-post-types'),
    "search_items" => __('search', 'myblueplanet-post-types'),
  );

  $args = array(
    "labels" => $labels,
    'public' => true,
    'has_archive' => false,
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'thumbnail',
      'custom-fields',
      'revisions',
      'templates'
    ),
    'menu_icon' => 'data:image/svg+xml;base64,' . base64_encode('<svg width="20" height="20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="utensils" class="svg-inline--fa fa-utensils fa-w-13" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 416 512"><path fill="currentColor" d="M207.9 15.2c.8 4.7 16.1 94.5 16.1 128.8 0 52.3-27.8 89.6-68.9 104.6L168 486.7c.7 13.7-10.2 25.3-24 25.3H80c-13.7 0-24.7-11.5-24-25.3l12.9-238.1C27.7 233.6 0 196.2 0 144 0 109.6 15.3 19.9 16.1 15.2 19.3-5.1 61.4-5.4 64 16.3v141.2c1.3 3.4 15.1 3.2 16 0 1.4-25.3 7.9-139.2 8-141.8 3.3-20.8 44.7-20.8 47.9 0 .2 2.7 6.6 116.5 8 141.8.9 3.2 14.8 3.4 16 0V16.3c2.6-21.6 44.8-21.4 48-1.1zm119.2 285.7l-15 185.1c-1.2 14 9.9 26 23.9 26h56c13.3 0 24-10.7 24-24V24c0-13.2-10.7-24-24-24-82.5 0-221.4 178.5-64.9 300.9z"></path></svg>'),
    "menu_position" => 5,
  );

  register_post_type('recipes', $args);
}
add_action('init', 'mbppt_recipe_post_type');

/* create custom taxonomy seasons */

function create_topics_hierarchical_taxonomy()
{

  $labels = array(
    'name' => _x('seasons', 'myblueplanet-post-types'),
    'singular_name' => _x('season', 'myblueplanet-post-types'),
    'search_items' =>  __('Search Seasons', 'myblueplanet-post-types'),
    'all_items' => __('All Seasons', 'myblueplanet-post-types'),
    'edit_item' => __('Edit Season', 'myblueplanet-post-types'),
    'update_item' => __('Update Season', 'myblueplanet-post-types'),
    'add_new_item' => __('Add New Season', 'myblueplanet-post-types'),
    'new_item_name' => __('New Season Name', 'myblueplanet-post-types'),
    'menu_name' => __('Seasons', 'myblueplanet-post-types')
  );

  register_taxonomy('seasons', array('recipes'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true
  ));
}
add_action('init', 'create_topics_hierarchical_taxonomy', 0);

/* ===== use custom flatsome template file inluded in this plugin for recepies ====== */
add_filter('single_template', 'mbppt_recipes_template');

function mbppt_recipes_template($single)
{

  global $post;

  // Checks for single template by post type
  if ($post->post_type == 'recipes') {
    return WP_PLUGIN_DIR . '/myblueplanet-post-types/mbppt-recipe-template.php';
  }

  return $single;
}

/* ===== Recipes output function for use in loops ===== */
function mbppt_recipe_output()
{ ?>
  <a class="portfolio-link" href="<?php the_permalink(); ?>">
    <div class="mbppt-thumbnail-wrapper"><div class="mbppt-thumbnail" style="background-image: url('<?php the_post_thumbnail_url("archive_thumbnail") ?>')"></div></div>
    <h3><?php the_title() ?></h3>
    <?php $post_id = get_the_ID(); ?>
    <div class="mbppt-container mbppt-stats">
      <!-- CO2 stats will be hidden/ displayed according to the selection in the form using js -->
      <div id="co2">
        <img class="mbppt-icon-archive" src="<?php echo get_site_url() ?>\wp-content\plugins\myblueplanet-post-types\assets\icons\outline-cloud-24px.svg" alt="CO2">
        <p class="co2-text-1">
          <?php
          echo get_post_meta($post_id, 'co2_summer', true) . 'g CO<sub>2</sub>';
          ?>
        </p>
        <p class="co2-text-2 mbppt-hidden">
          <?php
          echo get_post_meta($post_id, 'co2_autum', true) . 'g CO<sub>2</sub>';
          ?>
        </p>
        <p class="co2-text-3 mbppt-hidden">
          <?php
          echo get_post_meta($post_id, 'co2_winter', true) . 'g CO<sub>2</sub>';
          ?>
        </p>
        <p class="co2-text-4 mbppt-hidden">
          <?php
          echo get_post_meta($post_id, 'co2_spring', true) . 'g CO<sub>2</sub>';
          ?>
        </p>
      </div>
      <!-- time stats -->
      <div id="time">
        <img class="mbppt-icon-archive" src="<?php echo get_site_url() ?>\wp-content\plugins\myblueplanet-post-types\assets\icons\outline-av_timer-24px.svg" alt="Time">
        <p id="co2-text-4">
          <?php
          echo get_post_meta($post_id, 'time', true);
          ?>
        </p>
      </div>
      <!-- cost stats -->
      <div id="cost">
        <img class="mbppt-icon-archive" src="<?php echo get_site_url() ?>\wp-content\plugins\myblueplanet-post-types\assets\icons\outline-attach_money-24px.svg" alt="Cost">
        <p id="co2-text-4">
          <?php
          echo get_post_meta($post_id, 'cost', true);
          ?>
        </p>
      </div>
    </div>
  </a>
<?php }

/* ===== Create recipes archive loop shortcode ====== */
function mbppt_recipe_archive()
{
  ob_start();

  $query_recipes = new WP_Query(
    array(
      'post_type' => 'recipes',
      'posts_per_page' => -1
    )
  );
  ?>
  <!-- Season selection form for CO2 calculation -->
  <form autocomplete="off" id="mbppt_form" name="mbppt_form" class="mbppt-container">
    <label class="mbppt-check-container"><?php _e('Summer', 'myblueplanet-post-types'); ?>
      <input type="radio" checked="checked" name="radio" value="1">
      <span class="mbppt-radiomark"></span>
    </label>

    <label class="mbppt-check-container"><?php _e('Autum', 'myblueplanet-post-types'); ?>
      <input type="radio" name="radio" value="2">
      <span class="mbppt-radiomark"></span>
    </label>

    <label class="mbppt-check-container"><?php _e('Winter', 'myblueplanet-post-types'); ?>
      <input type="radio" name="radio" value="3">
      <span class="mbppt-radiomark"></span>
    </label>

    <label class="mbppt-check-container"><?php _e('Spring', 'myblueplanet-post-types'); ?>
      <input type="radio" name="radio" value="4">
      <span class="mbppt-radiomark"></span>
    </label>
  </form>
  <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" class="mbppt-recipe-ajax-filter" id="mbppt-ajax-filter">
    <?php
    if ($terms = get_terms(array('taxonomy' => 'seasons'))) :
      echo '<select name="categoryfilter">';
      foreach ($terms as $term) :
        echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
      endforeach;
      echo '</select>';
    endif;
    ?>
    <input type="text" class="send" name="keyword" id="keyword">
    <button class="mbppt-search"><i class="icon-search"></i></button>
    <input type="hidden" name="action" value="recipe_filter">
  </form>
  <div class="mbppt-archive" id="response">
    <div class="mbppt-hidden" id="mbppt-loader-wrapper"></div>
    <?php while ($query_recipes->have_posts()) : $query_recipes->the_post();

      echo mbppt_recipe_output();

    endwhile;
    wp_reset_query(); ?>
  </div>
  <?php
  // inject js function into footer area (function defined in mbp_klimaschule_post_types.php)
  add_action('wp_footer', 'mbppt_js');

  return ob_get_clean();
}
add_shortcode('mbppt_recipe_archive', 'mbppt_recipe_archive');

// Prossess ajax request
add_action('wp_ajax_recipe_filter', 'mbppt_recipes_filter_function'); // wp_ajax_{ACTION HERE} 
add_action('wp_ajax_nopriv_recipe_filter', 'mbppt_recipes_filter_function');

function mbppt_recipes_filter_function()
{
  // for taxonomies / categories
  $args = array(
    'post_type' => 'recipes',
    'posts_per_page' => -1,
    'post_status' => 'publish'
  );
  // if category filled, use category in query
  if (isset($_POST['categoryfilter']))
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'seasons',
        'field' => 'id',
        'terms' => $_POST['categoryfilter']
      )
    );
  // if keyword filled, use keyword in query
  if (isset($_POST['keyword']))
    $args['s'] = $_POST['keyword'];

  $query = new WP_Query($args);

  ?> <div class="mbppt-hidden" id="mbppt-loader-wrapper"></div> <?php
          if ($query->have_posts()) :
            while ($query->have_posts()) : $query->the_post();

              echo mbppt_recipe_output();

            endwhile;
            wp_reset_postdata();
          else :
            echo '<h3 class="noPostsFoundh3">'._e('No posts found', 'myblueplanet-post-types').'</h3>';
          endif;

          die();
        }

        /* ===== Create random recipes loop shortcode ====== */
        function mbppt_recipe_rand()
        {
          ob_start();
          $exclude_ids = array(get_the_ID());

          $query_recipes_rand = new WP_Query(
            array(
              'post_type' => 'recipes',
              'orderby' => 'rand',
              'posts_per_page' => 4,
              'post__not_in' => $exclude_ids
            )
          );
          ?>
  <!-- Season selection form for CO2 calculation -->
  <h4><?php _e('More recipes', 'myblueplanet-post-types'); ?></h4>
  <form autocomplete="off" id="mbppt_form" name="mbppt_form" class="mbppt-container">
    <label class="mbppt-check-container"><?php _e('Summer', 'myblueplanet-post-types'); ?>
      <input type="radio" checked="checked" name="radio" value="1">
      <span class="mbppt-radiomark"></span>
    </label>

    <label class="mbppt-check-container"><?php _e('Autum', 'myblueplanet-post-types'); ?>
      <input type="radio" name="radio" value="2">
      <span class="mbppt-radiomark"></span>
    </label>

    <label class="mbppt-check-container"><?php _e('Winter', 'myblueplanet-post-types'); ?>
      <input type="radio" name="radio" value="3">
      <span class="mbppt-radiomark"></span>
    </label>

    <label class="mbppt-check-container"><?php _e('Spring', 'myblueplanet-post-types'); ?>
      <input type="radio" name="radio" value="4">
      <span class="mbppt-radiomark"></span>
    </label>
  </form>
  <div class="mbppt-rand" id="response">
    <?php while ($query_recipes_rand->have_posts()) : $query_recipes_rand->the_post();

      echo mbppt_recipe_output();

    endwhile;
    wp_reset_query(); ?>
  </div>
  <?php
  // inject js function into footer area (function defined in mbp_klimaschule_post_types.php)
  add_action('wp_footer', 'mbppt_js');

  return ob_get_clean();
}
add_shortcode('mbppt_recipe_rand', 'mbppt_recipe_rand');
